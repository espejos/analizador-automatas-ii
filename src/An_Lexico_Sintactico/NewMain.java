package An_Lexico_Sintactico;

public class NewMain {

    static TablaLexema tablaLexemas = new TablaLexema();
    
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        
        new GeneradorEstructuras();
        new An_Sintactico();
        tablaLexemas.imprimirTabla();
        tablaLexemas.llamaTablaSimbolos();
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio));
        System.out.println("");
        System.out.println(tiempo +" milisegundos");
     
    }
}
